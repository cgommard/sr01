//
// Created by chloe on 18/10/2022.
//

#ifndef UNTITLED_TEST_H
#define UNTITLED_TEST_H
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#define MAX 200
#define MAX2 1000
typedef struct position{
    double x;
    double y;
} position;

typedef struct Restaurant {
    char  nom_restaurant [MAX2];
    char  adresse_restaurant[MAX2];
    position * position_restaurant;
    char  specialite[MAX2] ;
} Restaurant;

typedef struct Tableau {
    int nb;
    Restaurant restaurants[MAX];
} Tableau;

int lire_restaurant (char* chemin, Tableau * tableau);
void inserer_restaurant( Restaurant restaurant);
void cherche_restaurant(double x, double y, double rayon_recherche,Tableau * results) ;
void tri_tableau ( double x, double y,  Tableau * tableau);
void cherche_par_specialite(double x, double y, char specialite[MAX][MAX], Tableau * results);
void viderBuffer();
#endif //UNTITLED_TEST_H
