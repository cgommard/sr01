#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#define MAX 200;
#include "test.h"

char * chemin = "C:\\Users\\chloe\\OneDrive\\Bureau\\SR01\\untitled\\cmake-build-debug\\restau.txt";
Tableau * tableau ;
int nb_max;

void viderBuffer() {
    int c = '0';
    while (c!='\n' && c != EOF) { c = getchar(); }
}

int lire_restaurant (char* chemin, Tableau * tableau){
    int i =0;
    double a, b;
    FILE* restos;
    char* ligne= malloc(sizeof(char) * 1024);
    char* info;
    restos = fopen(chemin, "r");
    if (restos==NULL) {
        exit(EXIT_FAILURE);//test de l'ouverture du fichier
    }


    while (fgets( ligne , 1024 , restos) && tableau->nb<MAX) { //selectionne une ligne à parser

        info  = strtok(ligne, ";"); // segmentation de la ligne en parties séparées par des ";"

        for ( i =0; i<  4; i++){
            if (info!=NULL){
                if ( i==0){
                    strcpy ( tableau->restaurants[tableau->nb].nom_restaurant , info);
                    printf(" %s\n", tableau->restaurants[tableau->nb].nom_restaurant);

                }
                if (i==1){
                    strcpy ( tableau->restaurants[tableau->nb].adresse_restaurant , info);
                    printf(" %s\n", tableau->restaurants[tableau->nb].adresse_restaurant );
                }
                if ( i==2){
                    printf("celui la ");
                    sscanf(info,"(x= %lf, y= %lf)", &a ,&b );
                    tableau->restaurants[tableau->nb].position_restaurant->x = a;
                    tableau->restaurants[tableau->nb].position_restaurant->y = b;
                    printf ("%lf , %lf \n", tableau->restaurants[tableau->nb].position_restaurant->x,tableau->restaurants[tableau->nb].position_restaurant->y);
                }
                if (i==3){
                    info = strtok(strtok(info, "{"), "}"); // rajouté
                    strcpy ( tableau->restaurants[tableau->nb].specialite , info);
                    printf(" %s\n", tableau->restaurants[tableau->nb].specialite);
                }
            }
            info = strtok(NULL, ";");// selectionne le prochain segment
        }
        tableau->nb++;
        info =NULL;
    }
    fclose (restos);
    return tableau->nb;
}

void inserer_restaurant( Restaurant restaurant){
    FILE* fichier=fopen("restau.txt","a"); // on utilise fprintf pour insérer directement une chaine de caractère contenant tous les attributs
    char x [MAX] ;
    char y [MAX] ;
    sprintf(x, "%lf", restaurant.position_restaurant->x);//conversion de double vers un string
    sprintf(y, "%lf", restaurant.position_restaurant->y);
    fprintf(fichier, "\n%s; %s;(x=%s, y=%s);{%s};",restaurant.nom_restaurant, restaurant.adresse_restaurant, x,y,restaurant.specialite );// insertion en respectant le formatage du fichier
    fclose(fichier);
}

void cherche_restaurant(double x, double y, double rayon_recherche, Tableau * results){
    int i;
    int j = 0;
    for (i =0; i<tableau->nb; i++){
            if ( sqrt((tableau->restaurants[i].position_restaurant->x-x)*(tableau->restaurants[i].position_restaurant->x-x)+(tableau->restaurants[i].position_restaurant->y -y)*(tableau->restaurants[i].position_restaurant->y -y))<rayon_recherche)
            {
                results-> restaurants [j] = tableau->restaurants[i];
                j++;
            }
    }
    results->nb =j;
    printf("%d résultats : \n", results->nb);
    for (i=0; i<j; i++){
        printf ( "%s\n" , results->restaurants [i].nom_restaurant);
    }

}

void tri_tableau (double x, double y, Tableau * tableau){ // tri par sélection
    double min = 1000;// variable servant à retenir le minimum
    int indice = 0;
    int i =0;
    int j = 0;
    Restaurant  temp;
    for (j =0; j<tableau->nb; j++) {
        for (i =j; i<tableau->nb; i++) {

            if (min>sqrt((tableau->restaurants[i].position_restaurant->x-x)*(tableau->restaurants[i].position_restaurant->x-x)+(tableau->restaurants[i].position_restaurant->y-y)*(tableau->restaurants[i].position_restaurant->y-y)))
                {
                min = sqrt((tableau->restaurants[i].position_restaurant->x-x)*(tableau->restaurants[i].position_restaurant->x-x)+(tableau->restaurants[i].position_restaurant->y-y)*(tableau->restaurants[i].position_restaurant->y-y));
                indice = i;
            }

        }

        temp = tableau->restaurants[j];
        tableau->restaurants[j] = tableau->restaurants[indice];
        tableau->restaurants [indice] = temp;
        min = 1000;

    }
}

void cherche_par_specialite(double x, double y, char specialite[MAX][MAX], Tableau * results){
    int i =0;
    int j = 0;
    int k;
    for (i =0; i<tableau->nb; i++){
        for ( k =0; k< nb_max; k++) {
            if (strcmp(tableau->restaurants[i].specialite, specialite[k]) == 0) {//comparaison des chaines de caractères
                results->restaurants[j] = tableau->restaurants[i];
                j++;
            }
        }
    }
    results->nb =j;
    tri_tableau (x,y, results ); // appel de la fonction de tri pour afficher en fonction de la distance
    for (i=0; i<j; i++){
        printf ( "%s\n" , results->restaurants[i].nom_restaurant);
    }
}


int main() {
    Tableau * results;
    tableau = malloc (sizeof (Tableau));
    for ( int k = 0; k<MAX ; k++){
        tableau->restaurants[k].position_restaurant = malloc ( sizeof (position));
        printf ("%d" , k);
    }
    tableau->nb =0;

    Restaurant restaurant;
    char choixMenu = '0' ;
    char tmp[MAX];
    char specialite [MAX][MAX];
    double x, y , rayon_recherche;

    do {
        printf("\n========================================");
        printf("\n  [0] Lire un fichier");
        printf("\n  [1] Inserer un restaurant");
        printf("\n  [2] Chercher restaurants");
        printf("\n  [3] Rechercher par specialite");
        printf("\n  [9] Quitter");
        printf("\n========================================");
        printf("\nVotre choix : ");
        choixMenu = getchar();
        viderBuffer();



        switch (choixMenu) {
            case '0' :
                lire_restaurant ( chemin, tableau);
                break;

            case '1' :

                printf("Entrez le nom du restaurant :");
                fflush(stdin); // on vide le buffer après chaque "printf" pour ne pas fausser la saisie du "gets" qui vient juste après
                gets(tmp); strcpy(restaurant.nom_restaurant,tmp);
                printf("Entrez l'adresse du restaurant :");
                fflush(stdin);
                gets(tmp); strcpy(restaurant.adresse_restaurant,tmp);
                printf("Entrez la position du restaurant :");
                fflush(stdin);

                restaurant.position_restaurant = malloc ( sizeof (position));
                scanf("%lf", &restaurant.position_restaurant->x);
                printf("%lf",restaurant.position_restaurant->x );
                scanf("%lf", &restaurant.position_restaurant->y);
                printf("Entrez la specialite du restaurant :");
                fflush(stdin);
                gets(tmp); strcpy(restaurant.specialite,tmp);

                inserer_restaurant(restaurant);

                free(restaurant.position_restaurant);
                break;
            case '2' :
                results = malloc (sizeof (Tableau));
                for ( int k = 0; k<MAX ; k++){
                    results->restaurants[k].position_restaurant = malloc ( sizeof (position));
                }
                printf("Entrez la coordonnee x :");
                scanf("%lf", &x);
                printf("Entrez la coordonnee y :");
                scanf("%lf", &y);
                printf("Entrez le rayon de tolerance :");
                scanf("%lf", &rayon_recherche);
                cherche_restaurant(x,y, rayon_recherche, results);
                for ( int k = 0; k<MAX; k++){
                    free (results->restaurants->position_restaurant);
                }
                free (results);
                break;
            case '3' :
                results = malloc (sizeof (Tableau));
                for ( int k = 0; k<MAX ; k++){
                    results->restaurants[k].position_restaurant = malloc ( sizeof (position));
                }
                printf("Entrez la coordonnee x :");
                scanf("%lf", &x);
                printf("Entrez la coordonnee y :");
                scanf("%lf", &y);
                //scan le nom
                printf("Entrez le nombre de specialite recherchee :");
                scanf("%d",&nb_max);

                for (int j=0;j<nb_max;j++){
                    printf("Entrez le nom de la specialite sous la forme : {nom} ");
                    fflush(stdin);
                    gets(tmp); strcpy(specialite[j],tmp);
                }
                cherche_par_specialite(x,y,specialite,results);
                for ( int k = 0; k<MAX; k++){
                    free (results->restaurants[k].position_restaurant );
                }

                free (results);
                break;

        }
    }while (choixMenu != '9');

    for ( int k = 0; k<tableau->nb ; k++){
        free (tableau->restaurants->position_restaurant);
    }
    free (tableau);

    printf("\n\n*** FIN DU PROGRAMME ***\n");

    return 0;
}
