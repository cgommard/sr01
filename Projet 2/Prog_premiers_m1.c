#include<unistd.h>
#include<stdio.h>
#include<stdlib.h>
#include<sys/wait.h>
#include<string.h>
int premier(int nb)
{
    int r=0;

    for(int i = 1 ; i <= nb ; i++ )
    {
        if(nb % i == 0)
        {
           r++;
        }
    }

    if(r>2)
       return 0;//pas nb premier
    else
       return 1;//nombre premier

}
void explorer(int debut, int fin){
  int etat,pid,pid2,pidsys;
  pid=fork();
  if(pid==0){
    for (int i=debut; i<=fin;i++){

      if (premier(i)==1) {
        pid2=fork();
        fflush(stdout);//vide le buffer
        if (pid2==0){
          char chaine[100];
          sprintf(chaine,"echo ' %d  est un nombre premier \
          écrit par le processus %d, son pere est le processus %d et le PID du processus system est %d'>>nbr_premiers_m1.txt",i,getpid(), getppid(), getpid()+1);
          system(chaine);
          sleep(2);
          exit(0);
        }
        else wait(&etat);// instruction 41
      }

    }
    exit(0);

  } else wait(&etat);// instruction 46

}
int main(){
printf("\nLe PID du shell est : %d\n", getpid());
  int grp=1;
  while(grp<=11){
    explorer(grp+1,grp+10);
    grp=grp+10;// on analyse 10 par 10

  }
}
