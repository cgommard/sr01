
//////////
#include<unistd.h>
#include<stdio.h>
#include<stdlib.h>
#include<sys/wait.h>
#include<string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
void my_system( char* chaine){
    char *args[]={"/bin/echo",chaine,NULL};
    execv("/bin/echo",args);
}
int premier(int nb)
{
    int r=0;

    for(int i = 1 ; i <= nb ; i++ )
    {
        if(nb % i == 0)
        {
           r++;
        }
    }

    if(r>2)
       return 0;//pas nb premier
    else
       return 1;//nombre premier

}
void check_zombi(int p, int etat){
    //pid_t pid_f;
   waitpid (p, &etat, WNOHANG);
    printf("Le fils %d a terminé\n", p);

}
void explorer(int debut, int fin){
  int etat,pid,pid2;
  pid=fork();
  if(pid==0){
    for (int i=debut; i<=fin;i++){

      if (premier(i)==1) {
        pid2=fork();
        fflush(stdout);//vide le buffer
        if (pid2==0){
          char chaine[100];
          sprintf(chaine," %d  est un nombre premier \
          écrit par le processus %d, son pere est le processus %d et le PID du processus system est %d",i,getpid(), getppid(), getpid()+1);
          my_system(chaine);
          sleep(2);
          exit(0);
        }
        else check_zombi(pid2, etat);// instruction 41
      }

    }
    exit(0);

  } else check_zombi(pid, etat);// instruction 46

}
int main(){
    int fd;
  close(1);//pour fermer le stdout
  if((fd = open("nbr_premiers_m3.txt", O_RDWR | O_CREAT)==-1)){
    perror("open");
    return 1;
  }

  int grp=1;
  while(grp<=11){
    explorer(grp+1,grp+10);
    grp=grp+10;// on analyse 10 par 10

  }
}
