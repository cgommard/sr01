
#include<unistd.h>
#include<stdio.h>
#include<stdlib.h>
#include<sys/wait.h>
#include<string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

explorer(debut, fin){
  int etat,pid,pid2;
  pid=fork();
  if(pid==0){
    for (int i=debut; i<=fin;i++){

      if (premier(i)==1) {
        pid2=fork();
        fflush(stdout);//vide le buffer
        if (pid2==0){
          char chaine[100];
          sprintf(chaine," %d  est un nombre premiers \
          écrit par le processus %d, son pere est le processus %d et le PID du processus system est %d",i,getpid(), getppid(), getpid()+1);
          my_system(chaine);
          sleep(2);
          exit(0);
        }
        else check_zombi(pid2, &etat);// instruction 41
      }

    }
    exit(0);

  } else check_zombi(pid, &etat);// instruction 46

}
check_zombi( p, etat){
    //pid_t pid_f;
   waitpid (p, &etat, WNOHANG);
    printf("Le fils %d a terminé\n", p);

}
